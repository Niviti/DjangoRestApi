from rest_framework import serializers

from .models import Comment, OfferModel


class OfferSerializer(serializers.Serializer):
       id = serializers.IntegerField()
       author = serializers.IntegerField()
       item_popyt = serializers.CharField(max_length=300)
       item_podaz = serializers.CharField(max_length=300)
       room = serializers.CharField(max_length=4)
       description = serializers.CharField(max_length=500)
       hostel = serializers.CharField(max_length=500, default="Limba") 

       def create(self, validated_data):

            return OfferModel.objects.create(**validated_data)
     
       def update(self, instance, validated_data):
                """
                Update and return an existing `Snippet` instance, given the validated data.
                """
                instance.author = validated_data.get('author', instance.author)
                instance.item_popyt = validated_data.get('item_popyt', instance.item_popyt)
                instance.item_podaz = validated_data.get('item_podaz', instance.item_podaz)
                instance.room = validated_data.get('room', instance.room)
                instance.description = validated_data.get('description', instance.description)
                instance.save()
                
                return instance

class CommentSerializer(serializers.Serializer):
      offer = serializers.IntegerField()
      author = serializers.IntegerField()
      description = serializers.CharField(max_length=500)
      
      def create(self, validated_data):
              """
              Create and return a new `Snippet` instance, given the validated data.
              """
              return Comment.objects.create(**validated_data)
      
      def update(self, instance, validated_data):

              instance.offer = validated_data.get('offer', instance.offer)
              instance.author = validated_data.get('author', instance.author)
              instance.description = validated_data.get('description', instance.description)
              instance.save()
                
              return instance