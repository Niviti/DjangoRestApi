from django.conf.urls import url
from . import views

from .views import HomeTemplate, GetAllOffer, GetAllComments, OfferTemplate, CommentTemplate, PostOffer, PostComments, PutOffer, DeleteOffer, PutComments, DeleteComments, UpdateOfferTemplate, DeleteOfferTemplate, UpdateCommentTemplate, DeleteCommentTemplate


urlpatterns = [
    # example of how to static template from django just for fun
    url('api/post/offer/$', PostOffer),
    url('api/post/comment/$', PostComments),
    url('api/offer/(?P<hostel>[-\w]+)/$', GetAllOffer),
    url('api/offer/update/(?P<pk>[\d-]+)/$', PutOffer),
    url('api/offer/delete/(?P<pk>[\d-]+)/$', DeleteOffer),
    url('api/comments/get/(?P<pk>[\d-]+)/$', GetAllComments),
    url('api/comments/update/(?P<pk>[\d-]+)/$', PutComments),
    url('api/comments/delete/(?P<pk>[\d-]+)/$', DeleteComments),
    url('offer/$', OfferTemplate),
    url('offer/update/(?P<pk>[\d-]+)/$', UpdateOfferTemplate.as_view()),
    url('offer/delete/(?P<pk>[\d-]+)/$', DeleteOfferTemplate.as_view()),
    url('comment/$', CommentTemplate),
    url('comment/update/(?P<pk>[\d-]+)/$', UpdateCommentTemplate.as_view()),
    url('comment/delete/(?P<pk>[\d-]+)/$', DeleteCommentTemplate.as_view()),
    url('', HomeTemplate.as_view(template_name='home.html'), name='homepage'),
]

