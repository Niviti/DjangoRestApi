# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from myapp.models import OfferModel, Comment


# Register your models here.
admin.site.register(OfferModel)
admin.site.register(Comment)
