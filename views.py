# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.template import loader
from django.views.generic.base import TemplateView

from rest_framework.response import Response
from rest_framework.decorators import api_view

from .models import OfferModel,Comment
from django.http import JsonResponse
from django.http import HttpResponse

from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.serializers.json import DjangoJSONEncoder
from .serializers import OfferSerializer 
from rest_framework import status

from.forms import CommentForm, OfferModelForm
from .models import OfferModel, Comment
from django.views.generic.edit import UpdateView, DeleteView
from django.urls import reverse

class HomeTemplate(TemplateView):
    template_name = "home.html"
    
class DeleteCommentTemplate(DeleteView):
      model = Comment 
      
      def get_success_url(self):
               return reverse('homepage') 

class DeleteOfferTemplate(DeleteView):
      model = OfferModel   
      
      def get_success_url(self):
               return reverse('homepage') 
       
class UpdateCommentTemplate(UpdateView):
    template_name = "comment_form.html"
    model = Comment 
    form_class = CommentForm

class UpdateOfferTemplate(UpdateView):
    template_name = "offer_form.html"
    model = OfferModel 
    form_class = OfferModelForm    

def OfferTemplate(request):
    template_name = "offer_form.html"

    form = OfferModelForm() 

    return render(request, 'offer_form.html', {"form": form } )

def CommentTemplate(request):
    template_name = "comment_form.html"

    form = CommentForm(request.POST or None) 
    
    return render(request, 'comment_form.html', {"form": form } )


@api_view(['DELETE'])
def DeleteOffer(request, *args, **kwargs):       
          
          Post = OfferModel.objects.get(id= kwargs['pk'])
          
          Post.delete()
          return Response(status=status.HTTP_201_CREATED)
          

@api_view(['DELETE'])
def DeleteComments(request, *args, **kwargs):       
          
          comments = Comment.objects.get(id=kwargs['pk'])
          
          comments.delete()
          return Response(status=status.HTTP_201_CREATED)   


@api_view(['PUT'])
def PutComments(request, *args, **kwargs):       
          
          Comments = Comment.objects.get(id= kwargs['pk'])
          Comments['description'] = request.data['request']
          
          Comments.save()
          return Response(status=status.HTTP_201_CREATED)
           




@api_view(['PUT'])
def PutOffer(request, *args, **kwargs):       
          print(kwargs['pk'])
          Post = OfferModel.objects.get(id= kwargs['pk'])
          Post.item_podaz = request.data['item_popyt']
          Post.item_popyt = request.data['item_podaz']
          Post.room = request.data['room'] 
          Post.description = request.data['description']
          
          Post.save()  
          return Response(status=status.HTTP_201_CREATED)
      



@api_view(['POST'])
def PostOffer(request):       
          
          form = OfferModelForm(data=request.data)
          
          if form.is_valid():
              form.save()
              return Response(form.data, status=status.HTTP_201_CREATED)
 
          return Response(form.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def PostComments(request):       
          
          form = CommentForm(data=request.data)

          if form.is_valid():
             form.save()
             return Response(form.data, status=status.HTTP_201_CREATED)
          
          return Response(form.data, status=status.HTTP_400_BAD_REQUEST)   
  
 
@api_view(['GET'])
def GetAllOffer(APIView, *args, **kwargs):       
          
          query = OfferModel.objects.filter(hostel=kwargs['hostel']).values()
          
          data = {'offer': query} 

          return Response(data)

@api_view(['GET'])
def GetAllComments(APIView, *args, **kwargs):       
          
         ## Ouruser=User.objects.filter(username=request.user.username)
          
         ## id=Ouruser[0].id

          query = Comment.objects.filter(offer=kwargs['pk']).values()
        
          data = {'comment': query} 

          return Response(data)          