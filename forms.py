
from django import forms
from .models import OfferModel, Comment


class OfferModelForm(forms.ModelForm):
    
    class Meta:
            model = OfferModel
            ## jeśli coś jest autocreated w modelu nie mozemy tego wsadzić do fields
            ## w modelu moze byc 10 rzeczy ale wyswietli sie tylko to co jest w fields
            fields = [
                "author",
                "item_popyt",
                "item_podaz",
                "room",
                "description",
                "hostel"
               ]
    
    #def __init__(self, *args, **kwargs):
    #        super(OfferModelForm, self).__init__(*args, **kwargs)
    


class CommentForm(forms.ModelForm):
    
    class Meta:
            model =  Comment
            ## jeśli coś jest autocreated w modelu nie mozemy tego wsadzić do fields
            ## w modelu moze byc 10 rzeczy ale wyswietli sie tylko to co jest w fields
            fields = [
                "offer",
                "author",
                "description"
               ]               
  
    def __init__(self, *args, **kwargs):
            super(CommentForm, self).__init__(*args, **kwargs)           