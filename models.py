# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib import auth
from django.conf import settings

# Create your models here.


class OfferModel(models.Model): 
       id = models.AutoField(primary_key=True)
       author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
       item_popyt = models.CharField(max_length=300)
       item_podaz = models.CharField(max_length=300)
       room = models.CharField(max_length=4)
       description = models.CharField(max_length=500)
       hostel = models.CharField(max_length=500, default="Limba") 

       def __int__(self):
            return self.id

       class Meta:
              unique_together = (("author", "item_popyt", "item_podaz", "room"),) 

class Comment(models.Model):
      id = models.AutoField(primary_key=True)
      offer = models.ForeignKey(OfferModel, on_delete=models.CASCADE)
      author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
      description = models.CharField(max_length=500)
      
    